package com.example.julian.mapstest;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    String tempName;
    Double templat = Double.valueOf(0);
    Double templong = Double.valueOf(0);
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
       LoadData();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 10));


    }

    private void LoadData(){

        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();

        MaxDatabase.child("FuturePlaces").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {

                        if (InnerSnapshop.getKey().equals("Lat")) {
                            templat = InnerSnapshop.getValue(Double.class);

                        }
                        if (InnerSnapshop.getKey().equals("Long")) {
                            templong = InnerSnapshop.getValue(Double.class);
                        }
                        if (InnerSnapshop.getKey().equals("Name")) {
                            tempName = InnerSnapshop.getValue(String.class);
                        }

                    }
                    // Add Marker

                    AddMarker(templat, templong, tempName);
                    System.out.println("YES") ;
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void AddMarker(Double Lat, Double Long, String Name) {
        LatLng location = new LatLng(Lat, Long);
        mMap.addMarker(new MarkerOptions().position(location).title(Name));

    }
}
